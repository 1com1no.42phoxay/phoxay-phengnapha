const int redPin = 9;    
const int greenPin = 10;  
const int bluePin = 11;   

void setup() {
  
  Serial.begin(9600);
  
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  
 
  digitalWrite(redPin, LOW);
  digitalWrite(greenPin, LOW);
  digitalWrite(bluePin, LOW);
}

void loop() {
     //
  if (Serial.available() > 0) {
    
    String input = Serial.readStringUntil('\n'); 

    
    input.trim();

    
    digitalWrite(redPin, LOW);
    digitalWrite(greenPin, LOW);
    digitalWrite(bluePin, LOW);
    
    if (input == "r") { // ສີແດງ
      digitalWrite(redPin, HIGH);
    } else if (input == "g") { // ສີຂຽວ
      digitalWrite(greenPin, HIGH);
    } else if (input == "b") { // ສີຟ້າ
      digitalWrite(bluePin, HIGH);
    } else if (input == "on") { // ສີຂາວ
      digitalWrite(redPin, HIGH);
      digitalWrite(greenPin, HIGH);
      digitalWrite(bluePin, HIGH);
    } else if (input == "off") { // ປິດ
      
    } else {
      
    }
  }
}